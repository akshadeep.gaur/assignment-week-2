package com.greatLearning.assignment2;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Employee> employees = new ArrayList<>();
	
		Employee e1 = new Employee();
		e1.setId(1);
		e1.setName("Aman");
		e1.setAge(20);
		e1.setSalary(1100000);
		e1.setDepartment("IT");
		e1.setCity("Delhi");
		System.out.println(e1);
		
		Employee e2 = new Employee();
		e2.setId(2);
		e2.setName("Bobby");
		e2.setAge(22);
		e2.setSalary(500000);
		e2.setDepartment("HR");
		e2.setCity("Bombay");
		System.out.println(e2);
		
		Employee e3 = new Employee();
		e3.setId(3);
		e3.setName("Zoe");
		e3.setAge(20);
		e3.setSalary(750000);
		e3.setDepartment("Admin");
		e3.setCity("Delhi");
		System.out.println(e3);
		
		Employee e4 = new Employee();
		e4.setId(4);
		e4.setName("Smitha");
		e4.setAge(21);
		e4.setSalary(1000000);
		e4.setDepartment("IT");
		e4.setCity("Chennai");
		System.out.println(e4);
		
		Employee e5 = new Employee();
		e5.setId(5);
		e5.setName("Smitha");
		e5.setAge(24);
		e5.setSalary(1200000);
		e5.setDepartment("HR");
		e5.setCity("Bengaluru");
		System.out.println(e5);
		
		employees.add(e1);
		employees.add(e2);
		employees.add(e3);
		employees.add(e4);
		employees.add(e5);
		
		for(Employee emp : employees) {
			if(emp.getId()<0) {
				throw new IllegalArgumentException("Illegal Argument Exception");
			}if(emp.getName()==null) {
				throw new IllegalArgumentException("Illegal Argument Exception");
			}if(emp.getAge()<0) {
				throw new IllegalArgumentException("Illegal Argument Exception");
			}if(emp.getSalary()<0) {
				throw new IllegalArgumentException("Illegal Argument Exception");
			}if(emp.getDepartment()==null) {
				throw new IllegalArgumentException("Illegal Argument Exception");
			}if(emp.getCity()==null) {
				throw new IllegalArgumentException("Illegal Argument Exception");
			}
		}
		
		System.out.println("\nNames of all employees in the sorted order are :");
		DataStructureA da = new DataStructureA();
		da.sortingNames(employees);
		
		System.out.println("\n\nCount of Employees from each city:");
		DataStructureB db = new DataStructureB();
		db.cityNameCount(employees);
		
		System.out.println("\nMonthly Salary of employee along with their ID is:");
		db.monthlySalary(employees);

	}

}
