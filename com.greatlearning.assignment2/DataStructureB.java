package com.greatLearning.assignment2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		// Storing city into the ArrayList
		ArrayList<String> city = new ArrayList<String>();
		for (Employee emp : employees) {
			city.add(emp.getCity());
		}
		// Using TreeMap for printing in alphabetical order like "{ , }"
		Map<String, Integer> counts = new TreeMap<String, Integer>();

		// Counting similar City
		for (String str : city) {
			if (counts.containsKey(str)) {
				counts.put(str, counts.get(str) + 1);
			} else {
				counts.put(str, 1);
			}
		}

		for (Map.Entry<String, Integer> entry : counts.entrySet()) {
			counts.put(entry.getKey(), entry.getValue());
		}
		System.out.println(counts);
	}

//Method for Calculating Monthly Salary
	public void monthlySalary(ArrayList<Employee> employees) {
		HashMap<Integer, Float> hm = new HashMap<Integer, Float>();
		for (Employee e : employees) {
			float salary = e.getSalary() / 12;
			hm.put(e.getId(), salary);
		}
		System.out.println(hm);

	}

}
